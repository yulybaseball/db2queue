import logging

LOG_FILE = 'logger.log'

class FormatterNotFormattingInfo(logging.Formatter):
    '''
    Create a separate formatter for messages of type INFO and the rest.
    '''
    def __init__(self, fmt='%(asctime)s - %(levelname)s - %(message)s'):
        logging.Formatter.__init__(self, fmt)

    def format(self, record):
        if record.levelno == logging.INFO:
            return record.getMessage()
        return logging.Formatter.format(self, record)

def load_logger(log_file_name=LOG_FILE, logger_name=__name__):
    '''
    Create logger.
    Use file defined in LOG_FILE to save logs.
    '''
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    if not logger.handlers:
        handler_info = logging.FileHandler(log_file_name)
        handler_info.setFormatter(FormatterNotFormattingInfo())
        logger.addHandler(handler_info)
    return logger