import ConfigParser
import datetime
import json
import io
import os
import pipes
import shlex
import subprocess
import time

from lxml import etree as ET

from logger.logger import load_logger

CONF_FILE = os.path.join(os.path.dirname(
                         os.path.realpath(__file__)), '.conf')
LOG_FILE_NAME = os.path.join(os.path.dirname(
                             os.path.realpath(__file__)), 'db2queues.log')

logger = None
PATCHES = {'=&gt;': '=>', '&gt;=': '>='}

def start():
    """Receive the logger object and print the initial time to log file."""
    global logger
    logger = load_logger(log_file_name=LOG_FILE_NAME)
    logger.info('============================================================')
    logger.info(datetime.datetime.now())

def quit_script(response):
    """Print datetime into log file.
    Call _reply function in order to send message back to request.
    Quit current script."""
    logger.info('Quiting now...')
    logger.info(datetime.datetime.now())
    logger.info('============================================================')
    _reply(response)
    exit()

def _reply(response):
    """If response is not a dictionary, create one and add the response to it.
    If response is already a dictionary, add the 'OK' message to it.
    Convert the response into JSON format and print it back as reply to the 
    the request."""
    if not isinstance(response, dict):
        response = {'message': response}
    else:
        response['message'] = 'OK'
    print json.dumps(response)

def get_conf_data():
    """Get the conf data stored in CONF_FILE. If expected data is not found, 
    it quits script. Return a dictionary containing that data.
    """
    confvalues = {}
    try:
        config = ConfigParser.ConfigParser()
        logger.info("Trying to read the conf file: {0}".format(CONF_FILE))
        config.read(CONF_FILE)
        logger.info("File was read.")

        confvalues['sides'] = [
            {'name': 'mia', 'servers': load_files('mia', config)}, 
            {'name': 'atl', 'servers': load_files('atl', config)}]

    except Exception, e:
        err_mes = "Exception ocurred: {0}".format(e)
        logger.error(err_mes)
        quit_script(err_mes)

    return confvalues

def _get_xml_conf_file_content(server_name, db2q_conf_f):
    """Return the content of the passed file in the passed server."""
    return cmd('ssh {0} "cat {1}"'.format(server_name, db2q_conf_f))

def _get_query_rows(query):
    ns_query = ''.join(query.lower().split())
    index = ns_query.rfind("rownum<")
    lessthan = 1
    sliceindex = len("rownum<")
    if index == -1:
        lessthan = 0
        sliceindex = len("rownum<=")
        index = ns_query.rfind("rownum<=")
        if index == -1:
            return ""
    realindex = index + sliceindex
    rowscount = "0"
    while realindex < len(ns_query) and ns_query[realindex].isdigit():
        rowscount += str(ns_query[realindex])
        realindex += 1
    rowscount = int(rowscount) - lessthan
    return rowscount

def load_files(side, config):
    """Create and return a list of servers in a specific side.
    Each server has a list of files with names and instances.

    Keyword arguments:
    side -- 'mia' or 'atl'
    config -- configuration file instance to read the servers from
    """
    logger.info("Loading files for {0}".format(side))
    side_lower = side.lower()
    if side_lower not in ['mia', 'atl']:
        err = "Unknown side to look for servers: {0}".format(side)
        logger.error(err)
        quit_script(err)
    side = 0 if side_lower == 'mia' else 1
    result = []
    try:
        ssl = list(config.items(side_lower, 'servers')[0])
        for server_name in ssl[1].strip().split('\n'):
            server = {'name': server_name, 'files': []}
            # get the applications.ALL file content (once per server)
            command = ("ssh {0} \"cd /home/weblogic; " + 
                    "cat applications.ALL | grep ':1' | cut -f2 -d:\"")
            applications_all_file = cmd(command.format(server_name))
            # load files for current server
            ifl = list(config.items('integration', 'files')[0])
            for db2q_conf_f in ifl[1].strip().split('\n'):
                # connect to server and cat the file
                xml_str = _get_xml_conf_file_content(server_name, db2q_conf_f)
                if xml_str: # file exists
                    # check if file is in use
                    pucf = processes_using_conf_file(
                        server_name, applications_all_file, db2q_conf_f)
                    # get instances in file
                    instances = []
                    root = ET.fromstring(xml_str)
                    for carrier in root.findall('./SystemProperties/Carrier'):
                        instance = {
                            'name': carrier.attrib['id'], 
                            'enabled' : carrier.find('Instance').attrib['enabled'],
                            'cycle': carrier.find('Instance').attrib['exectime'],
                            'rows': _get_query_rows(carrier.find('SQLQuery').text),
                        }
                        instances.append(instance)
                    # append the file
                    path_name = path_name_file(db2q_conf_f)
                    server['files'].append({
                        'name': path_name[1], 'path': path_name[0],
                        'active': bool(pucf), 'used_by': pucf, 
                        'instances': instances
                    })
            result.append(server)
        logger.info("Files were loaded:\n{0}\n".format(result))
    except Exception, e:
        err = "An exception ocurred: {0}".format(e)
        logger.error(err)
        quit_script(err)
    return result

def path_name_file(f):
    """Split complete file path into path and file name.
    Return tuple with both.

    Keyword arguments:
    f -- complete file path
    """
    s_f = f.split('/')
    path = "/".join(s_f[:-1])
    name = s_f[-1]
    return (path, name)

def processes_using_conf_file(server, applications_all_file, db2q_conf_f):
    """Create and return a list containing the complete path of the processes 
    currently running in server passed as parameter, and using the file 
    db2q_conf_f.
    """
    command = ""
    file_name = db2q_conf_f.split('/')[-1]
    for app_path in applications_all_file.split('\n'):
        if file_in_application_path(db2q_conf_f, app_path):
            command += (app_path + " ")
    if command: # we have data to execute grep command
        command = ("ssh {0} \"grep -l /config/{1} {2}\" ").\
                    format(server, file_name, command)
        result = cmd(command)
        if result:
            result = result.split('\n')
            return [proc for proc in result if proc]
    return []

def file_in_application_path(db2q_conf_f, app_path):
    """Compare db2queue file complete path with application complete path.
    If they match, return True. False otherwise.
    """
    spl_db2q_conf_f = db2q_conf_f.split('/')
    spl_app_path = app_path.split('/')
    # assuming db2queue config file root path is in this form:
    # /<something>/<any>/<times>/config/<config_file_name.xml>
    db2q_conf_f_root = "/".join(spl_db2q_conf_f[:-2])
    # assuming application file root path is in this form:
    # /<something>/<any>/<times>/<application_file_name.sh>
    app_path_root = "/".join(spl_app_path[:-1])
    return db2q_conf_f_root == app_path_root

def cmd(command, **kwargs):
    """Execute command and return the output.
    Can be used to write to stdin."""
    logger.info("Executing this command: {0}".format(command))
    p = subprocess.Popen(shlex.split(command), stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if kwargs:
        # send something to standard input
        if 'stdin' in kwargs:
            logger.info("Copying to stdin...")
            p.stdin.write(kwargs['stdin'])
    (stdout, stderr) = p.communicate()
    return stdout

def check_params(params):
    """Check script was called with 2 params. If not, quit script.
    Return the param as python object (list)."""
    logger.info("Checking params...")
    if len(params) != 3:
        err = "Wrong number of parameters."
        logger.error(err)
        quit_script(err)
    pv = json.loads(params[1])
    ip = params[2]
    logger.info(("There are 2 params:" + 
                 "\n\tValue: {0}\n\tType: {1}\n\tValue: {2}\n\tType: {3}").
                format(pv, type(pv), ip, type(ip)))
    logger.info("CONNECTION FROM IP: {0}".format(ip))
    return pv

def _group_instances_by_file(instances):
    """Create and return a dictionary containing the instances passed,
    but groupep up by file, in the form:
    {server@path@file: {instance_name: active(True/False), ...}, {...}}
    """
    result = {}
    for entry in instances:
        ins_spl = entry['name'].split('@')
        # take the first 3 elements: server, path and file name
        server_path_file = "@".join(ins_spl[:3])
        if server_path_file not in result:
            result[server_path_file] = {}
        result[server_path_file][ins_spl[3]] = entry['active']
    return result

def _patch_replace(filecontent):
    """Replace in filecontent every key found in PATCHES by its value."""
    for key in PATCHES:
        filecontent = filecontent.replace(key, PATCHES[key])
    return filecontent

def _write_conf_file(server, path, file_name, file_content):
    """Send command to create backup file and save the changes into server."""
    try:
        cd_cmd = "cd {0}".format(path)
        tm = time.strftime("%Y%m%d%H%M")
        backup_cmd = "cp {0} {1}.bak.{2}".format(file_name, file_name, tm)
        save_changes_cmd = "cat - > {0}".format(file_name)
        cmd('ssh {0} "{1}; {2}; {3}"'.format(server, cd_cmd, backup_cmd, 
                                             save_changes_cmd), 
            stdin=_patch_replace(file_content))
        logger.warning(("File {0}/{1} at {2} was updated!").
                        format(path, file_name, server))
    except Exception, e:
        err_msg = (("Following error happened when updating the content of " + 
                    "{0}/{1} from {2}: {3}").format(path, file_name, 
                                                    server, str(e)))
        logger.error(err_msg)
        quit_script(err_msg)

def _get_xml_root(server, full_file_path):
    """Connect to server, get file content, parse it as XML and return it."""
    root = None
    try:
        # get file content from server
        file_content = _get_xml_conf_file_content(server, full_file_path)
        root = ET.fromstring(file_content)
    except Exception, e:
        err_msg = (("Following error happened when getting XML content of " +
                "{0} from {1}: {2}").format(full_file_path, server, str(e)))
        logger.error(err_msg)
        quit_script(err_msg)
    return root

def _update_db2q_file(server_path_file, ins_values):
    """Get the file content from server and update it locally with the 
    values that changed."""
    spf_spl = server_path_file.split('@')
    server = spf_spl[0]
    path = spf_spl[1]
    ins_file = spf_spl[2]
    full_file_path = "/".join(spf_spl[1:3])
    root = _get_xml_root(server, full_file_path)
    xpath = ".//SystemProperties/Carrier"
    instance_names = ins_values.keys()
    try:
        for carrier in root.findall(xpath):
            carrier_id = carrier.attrib.get('id')
            if carrier_id in instance_names:
                # remove element so 'for' loop doesn't go through all instances
                # in conf file
                instance_names.remove(carrier_id)
                active = str(ins_values[carrier_id]).lower()
                xml_instance = carrier.find('Instance')
                xml_instance.set('enabled', active)
                logger.info(("Updating {0} to {1} in server {2}, " +
                            "file {3}").format(carrier_id, active.upper(), 
                                                server, full_file_path))
            if not instance_names:
                break
    except Exception, e:
        err_msg = (("Following error happened when updating the content " + 
                    "for {0}: {1}").format(server_path_file, str(e)))
        logger.error(err_msg)
        quit_script(err_msg)
    # if everything went OK, write the file
    _write_conf_file(server, path, ins_file, 
                    ET.tostring(root, xml_declaration=True, encoding="UTF-8"))

def update_instances(instances):
    """Group instances by server and update them on real files."""
    g_instances = _group_instances_by_file(instances)
    logger.info("Updating instances...")
    err_msg = ""
    for server_path_file, ins_values in g_instances.iteritems():
        try:
            _update_db2q_file(server_path_file, ins_values)
        except Exception, e:
            em = str(e)
            logger.error(em)
            err_msg += (em + '\n')
    result_msg = "Updating instances... FINISHED"
    logger.info(result_msg)
    return result_msg if not err_msg else err_msg
